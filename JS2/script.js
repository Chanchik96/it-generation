// 1. Среди трех чисел найти среднее арифметическое. Если среди чисел есть равные, вывести сообщение "Ошибка". Числа принять от пользователя.


function avgNum() {
  let num1 = +prompt("Введите первое число:");
  let num2 = +prompt("Введите второе число:");
  let num3 = +prompt("Введите третье число:");

  const avgNum = (num1 + num2 + num3) / 3;
  
  if(num1 === num2 || num1 === num3 || num2 === num3) {
    alert("Ошибка");
  } else {
    alert(`Среднее арифметическое чисел ${num1}, ${num2} и ${num3} равна ${avgNum}`);
  }
}

// avgNum();


// 2. Построить прямоугольный треугольник состоящий из символа "*".


function starTriangle() {
  const num = +prompt("Задайте количество звёздочек, которые будут размещаться по вертикали и горизонтали:");

  for(let i = 1; i <= num; i++) {
    for(let j = 0; j < i; j++) {
      document.write("*");
    }
    document.write("<br>")
  }
}

// starTriangle();


// 3. Определить какое из трех, введенных пользователем, чисел максимальное и вывести его на экран.


function maxNum() {
  let arr = prompt("Enter 3 numbers separated with commas:");
  if (arr === null) {
    return;
  }

  let k = true;
  while(k) {
    if (arr.split(",").length > 3) {
      alert("You've entered more than 3 numbers");
      arr = prompt("Enter 3 numbers separated with commas:");
    } else if (arr.split(",").length < 3) {
      alert("You've entered less than 3 numbers");
      arr = prompt("Enter 3 numbers separated with commas:");
    } else {
      break;
    }
  }

  const newArr = arr.split(",");
  let maxValue = Math.max(...newArr);
  alert(maxValue);
}

// maxNum();

// Вариант кода на ванилле
function maxNum1() {
  let arr = prompt("Enter 3 numbers separated with commas:");
  if (arr === null) {
    return;
  }

  let k = true;
  while(k) {
    if (arr.split(",").length > 3) {
      alert("You've entered more than 3 numbers");
      arr = prompt("Enter 3 numbers separated with commas:");
    } else if (arr.split(",").length < 3) {
      alert("You've entered less than 3 numbers");
      arr = prompt("Enter 3 numbers separated with commas:");
    } else {
      break;
    }
  }

  const newArr = arr.split(",");
  let maxValue = newArr[0];

  for(let i = 0; i < newArr.length; i++) {
    if(maxValue > newArr[i]) {
      continue;
    } else {
      maxValue = newArr[i];
    }
  }
  return alert(maxValue);
}

// maxNum1();


// 4. Дано два числа A и B где (A<B). 
//    Выведите на экран суму всех чисел расположенных в числовом промежутке от А до В. 
//    Выведите на экран все нечетные значения, расположенные в числовом промежутке от А до В. 


function myFunc() {
  let a = +prompt("Задайте число A:");
  let b = +prompt("Задайте число B (число B має бути більшим за число A):");
  if (a === null || b === null) {
    return;           //Якщо користувач натискає "Cancel", то ми виходимо із функції.
  }
  
  // Перевірка умови "A > B".
  let k = true;
  while(k) {
    if (b < a) {
      alert("Некоректні дані. Число A має бути більшим за число B");
      a = +prompt("Задайте число A:");
      b = +prompt("Задайте число B (число B має бути більшим за число A):");
    } else {
      break;
    }
  }

  let arr = [],
      oddNumArr = [],
      sum = 0;

  for(let i = a + 1; i < b; i++) {
    arr.push(i);
    sum += arr[arr.length - 1];

    if(arr[arr.length - 1] % 2 == 1) {
      oddNumArr.push(i);
    } else {
      continue;
    }
  }

  console.log(arr);
  console.log(oddNumArr);
  document.write(`Сума чисел, що розміщені у числовому проміжку від ${a} до ${b} дорювнює ${sum}` + "<br>");
  document.write(`Непарні числа, що розміщені у числовому проміжку від ${a} до ${b}: ${oddNumArr}`);
}

// myFunc();


//  5. Вывести на экран ряд чисел Фибоначчи, состоящий из n элементов (n принять от пользователя).
//     Числа Фибоначчи – это элементы числовой последовательности, в которой каждое последующее число равно сумме двух предыдущих.


function fibonacciSequence() {
  let n = prompt("Задайте натуральное число из которых будет состоять ряд чисел Фибоначчи:");
  n = Number(n);

  let k = true;
  while(k) {
    if(n < 1 || typeof(n) == NaN) {
      alert("Неверное значение! Повторите попытку.");
      n = prompt("Задайте натуральное число из которых будет состоять ряд чисел Фибоначчи:");
      n = Number(n);
    } else {
      break;
    }
  }

  let n1 = 0, n2 = 1, nextNum, seq = [];

  for(let i = 1; i <= n; i++) {
    console.log(n1);
    seq.push(n1);
    nextNum = n1 + n2;
    n1 = n2;
    n2 = nextNum;
  }

  return alert(seq);
}

// fibonacciSequence();

//  6. Вывести на экран таблицу умножения от 0 до 9.


function multiplicationTable() {
  document.write("<table>");
  for(i = 1; i < 10; i++) {
    document.write("<td>");
    for(j = 1; j <= 10; j++) {
      document.write(`${j} x ${i} = ${i * j}`);
      document.write("<br>");
    }
    document.write("</td>");
  }
  document.write("</table>");
}

// multiplicationTable();