// 1. По координатам двух точек, которые вводит пользователь, определить уравнение прямой, проходящей через эти точки. Общий вид уравнения: y = kx + b; где k = (y1 - y2) / (x1 - x2), b = y2 - k*x2.


var x1 = +prompt("x1?");
var x2 = +prompt("x2?");
var y1 = +prompt("y1?");
var y2 = +prompt("y2?");

var k = (y1 - y2) / (x1 - x2);
var b = y2 - k*x2;

if (x1 === x2) {
  document.write("You cannot divide by 0");
} else {
  if (b < 0) {
    document.write(`Рівняння прямої на площині з точками х1: ${x1}, х2: ${x2}, y1: ${y1}, y2: ${y2} :<b> y = ${k}x - ${-b}</b>`);
  } else {
    document.write(`Рівняння прямої на площині з точками х1:${x1}, х2:${x2}, y1:${y1}, y2:${y2} :<b> y = ${k}x + ${b}</b>`);
  }
}


// 2. Обменять значения двух переменных, используя третью (буферную) переменную.


var a = "Straight"
    b = "Gay";

console.log(a, b);

var temp = a;
a = b;
b = temp;

console.log(a, b);
    

// 3. Объявите две переменные: admin и name. Запишите в name строку "Василий". Замените значение из name в admin. Выведите admin.


var admin;
var name;

name = "Василий";
admin = name;

console.log(admin);


// 4. Получить от пользователя данные о пользователе: имя, фамилию, возраст. Обработать их и вывести на экран.


var fName = prompt("Вкажіть імʼя:");
var lName = prompt("Вкажіть прізвище:");
var age = +prompt("Вкажіть вік:");

document.write(`Привіт ${fName} ${lName}, якому ${age}! Рад познайомитися з тобою!`);


// 5. Запросите у пользователя его возраст, после чего выведите на экран модальное окно с вопросом: "Ваш возраст ...(введеное пользователем число) лет?". Далее в зависимости от выбора пользователя выводится модальное окно с информацией true или false.


function confirmAction() {
  var age = +prompt("Вкажіть вік:");

  let confirmAction = confirm(`Ваш вік ${age} років?`) ? alert("true") : alert("false");
}

confirmAction();