class Calculator {
  constructor(currentOperandTextElement) {
    this.currentOperandTextElement = currentOperandTextElement;
    this.clear();
  }

  clear() {
    this.currentOperand = '0';
    this.previousOperand = '';
    this.operation = undefined;
  }

  appendNumber(number) {
    if (number === '.' && this.currentOperand.includes('.')) return

    if (this.currentOperand === '0') {
      this.currentOperand = '';
    } else if (this.currentOperand === '.') {
      this.currentOperand = '0.';
    }
    this.currentOperand = this.currentOperand.toString() + number.toString();

    console.log(this.currentOperand);
  }

  addNegative() {
    this.currentOperand = this.currentOperand * -1;
  }

  chooseOperation(operation) {
    if (this.currentOperand === '') return;
    if (this.previousOperand !== '') {
      this.compute();
    }
    this.operation = operation;
    this.previousOperand = this.currentOperand;
    this.currentOperand = '';
  }

  compute() {
    let computation
    const prev = parseFloat(this.previousOperand);
    const current = parseFloat(this.currentOperand);
    if (isNaN(prev) || isNaN(current)) return;

    switch (this.operation) {
      case '+':
        computation = parseFloat((prev + current).toFixed(15));
        break;
      case '-':
        computation = parseFloat((prev - current).toFixed(15));
        break;
      case '*':
        computation = parseFloat((prev * current).toFixed(15));
        break;
      case '/':
        computation = parseFloat((prev / current).toFixed(15));
        break;
      default:
        return;
    }

    this.currentOperand = computation;
    this.operation = undefined;
    this.previousOperand = ''
  }

  percent() {
    this.currentOperand = this.currentOperand / 100
  }

  updateNumber(number) {
    const stringNumber = number.toString();
    const integerDigits = parseFloat(stringNumber.split('.')[0]);
    const decimalDigits = stringNumber.split('.')[1];
    let integerDisplay;

    if (isNaN(integerDigits)) {
      integerDisplay = '';
    } else {
      integerDisplay = integerDigits.toLocaleString('us');
    }

    if (decimalDigits != null) {
      return `${integerDisplay}.${decimalDigits}`;
    } else {
      return integerDisplay;
    }
  }

  updateDisplay() {
    this.currentOperandTextElement.innerHTML = this.updateNumber(this.currentOperand);
  }
}

const numberButtons = document.querySelectorAll('.number'),
      operationButtons = document.querySelectorAll('.operation'),
      equalsButton = document.querySelector('.equals'),
      clearButton = document.querySelector('.clear'),
      negativeButton = document.querySelector('.negative'),
      percentButton = document.querySelector('.percent'),
      currentOperandTextElement = document.querySelector('.result span');
      

const calculator = new Calculator(currentOperandTextElement);

numberButtons.forEach(button => {
  button.addEventListener('click', () => {
    calculator.appendNumber(button.value);
    calculator.updateDisplay();
  })
})

operationButtons.forEach(button => {
  button.addEventListener('click', () => {
    calculator.chooseOperation(button.value);
  })
})

equalsButton.addEventListener('click', () => {
  calculator.compute();
  calculator.updateDisplay();
})

clearButton.addEventListener('click', () => {
  calculator.clear();
  calculator.updateDisplay();
})

negativeButton.addEventListener('click', () => {
  calculator.addNegative();
  calculator.updateDisplay();
})

percentButton.addEventListener('click', () => {
  calculator.percent();
  calculator.updateDisplay();
})


