import React from 'react'
import ReactDOM from 'react-dom'
import data from './data'
import './index.css'


function App() {
  let dataTable = [];
  data.forEach((el, index) => {
    dataTable.push(
      <tr>
        <td key={index}>{el.r030}</td>
        <td key={index}>{el.cc}</td>
        <td key={index} className="align-left">{el.txt}</td>
        <td key={index} className="rate">{el.rate.toFixed(4)}</td>
      </tr>
    )
  })


  return (
    <>
      <h1>Офіційний курс гривні щодо іноземних валют</h1>
      <p>на дату {data[0].exchangedate}</p>
      <table>
        <thead>
          <tr>
            <th>Код цифровий</th>
            <th>Код літерний</th>
            <th>Назва валюти</th>
            <th>Офіційний курс</th>
          </tr>
        </thead>
        <tbody>
          {dataTable}
        </tbody>
      </table>
    </>
  )
}

ReactDOM.render(<App/>, document.getElementById("body"))