function showModalError(message) {
  Swal.fire({
    icon: "error",
    confirmButtonColor: "#ff8c42",
    title: `${message}`,
  });
}