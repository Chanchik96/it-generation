const className = 'active';
const scrollTrigger = 50;

window.onscroll = () => {
  if(window.onscroll >= scrollTrigger || window.scrollY >= scrollTrigger) {
    document.getElementsByTagName('header')[0].classList.add(className);
  } else {
    document.getElementsByTagName('header')[0].classList.remove(className);
  }
};

// document.getElementById('homeBtn').addEventListener('click', () => {
//   location.reload();
// });

document.querySelector('.logo').addEventListener('click', () => {
  location.reload();
});

document.getElementById('aboutBtn').addEventListener('click', () => {
  console.log('aboutBtn');
  let scrollDiv = document.getElementById('aboutMe').offsetTop;
  window.scrollTo({
    top: scrollDiv - 200,
    behavior: 'smooth'
  });
});

document.getElementById('skillsBtn').addEventListener('click', () => {
  console.log('skillsBtn');
  let scrollDiv = document.getElementById('skills').offsetTop;
  window.scrollTo({
    top: scrollDiv - 200,
    behavior: 'smooth'
  });
});

document.getElementById('portfolioBtn').addEventListener('click', () => {
  console.log('portfolioBtn');
  let scrollDiv = document.getElementById('portfolio').offsetTop;
  window.scrollTo({
    top: scrollDiv - 150,
    behavior: 'smooth'
  });
});

document.getElementById('contactBtn').addEventListener('click', () => {
  console.log('contactBtn');
  let scrollDiv = document.getElementById('contact').offsetTop;
  window.scrollTo({
    top: scrollDiv,
    behavior: 'smooth'
  });
});