const ball = document.querySelector(".ball");
const gate1 = document.getElementById("gate1");
const gate2 = document.getElementById("gate2");
const field = document.querySelector(".main");
const score = document.querySelector(".score");
const teamLeft = document.querySelector(".left");
const teamRight = document.querySelector(".right");

field.addEventListener("dragover", function (e) {
  e.preventDefault();
});

//Event on a drop to field and both gates
field.addEventListener("drop", dropBall);
gate1.addEventListener("drop", dropBall);
gate2.addEventListener("drop", dropBall);

function dropBall(e) {
  this.appendChild(ball);
  //correcting of ball coordinate
  ball.style.left = e.clientX - ball.offsetWidth / 2 + "px";
  ball.style.top = e.clientY - ball.offsetHeight / 2 + "px";
}

//Ball is back to the center
gate1.addEventListener("mouseleave", returnBallCenter);
gate2.addEventListener("mouseleave", returnBallCenter);

function returnBallCenter(e) {
  ball.style.top = 50 + "vh";
  ball.style.left = 50 + "%";
}

//Score of goals
gate1.addEventListener("dragenter", goalCount1);
gate2.addEventListener("dragenter", goalCount2);

let teamscore1 = 0;
let teamscore2 = 0;
function goalCount1() {
  teamscore1++;
  teamLeft.innerHTML = teamscore1;
}
function goalCount2() {
  teamscore2++;
  teamRight.innerHTML = teamscore2;
}
