const body = document.querySelector("body"),
      sidebar = document.getElementById("sidebar"),
      toggle = document.getElementById("toggle"),
      searchBtn = document.getElementById("searchBox"),
      modeSwitch = document.getElementById("toggleSwitch"),
      modeText = document.getElementById("modeText");

      
toggle.addEventListener('click', () => {
  sidebar.classList.toggle("close");
});

searchBtn.addEventListener('click', () => {
  sidebar.classList.remove("close");
})

modeSwitch.addEventListener('click', () => {
  body.classList.toggle("dark");

  if (body.classList.contains("dark")) {
    modeText.innerText = "Light Mode";
  } else {
    modeText.innerText = "Dark Mode";
  }
});