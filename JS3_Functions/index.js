/*
  1. Создать функцию calculate(operand1, operand2, sign), где operand1 и operand2 — два числа, sign — знак арифметической операции.
    Функция должна расчитывать результат операции, исходя из переданного ей знака. 
    Функция должна проверять корректность введенных чисел и знака операции.
    Все аргументы для функции принять от пользователя.

  2. Создать функцию, возводящую число в степень, число и сама степень вводится пользователем
    
  3. Создать игру "Камень-Ножницы-Бумага".
    Выбор компьютера определяется строкой:  
    "var computerChoice = Math.random();
    alert(computerChoice);".
    Math.random() выдает произвольное число в промежутке от 0 до 1, на основании этого можно построить принцип выбора решения компьютера.

  4. Напишите функцию, которая возвращает n-е число Фибоначчи. Для решения используйте цикл.

*/


// ========================== Task 1 ========================== //


function calculate(operand1, operand2, sign) {

  operand1 = +prompt("Operand 1?");
  if (operand1 === null) {
    return;
  }
  while (isNaN(operand1)) {
    operand1 = +prompt("Operand 1?");
  }

  operand2 = +prompt("Operand 2?");
  if (operand2 === null) {
    return;
  }
  while (isNaN(operand2)) {
    operand2 = +prompt("Operand 2?");
  }

  sign = prompt("Sign?");
  if (sign === null) {
    return;
  }
  while (sign !== "+" && sign !== "-" && sign !== "*" && sign !== "/") {
    sign = prompt("Sign?");
  }

  let result;
  switch (sign) {
    case "+":
      result = operand1 + operand2;
      break;
    case "-":
      result = operand1 - operand2;
      break;
    case "*":
      result = operand1 * operand2;
      break;
    case "/":
      result = operand1 / operand2;
      if (operand2 === 0) {
        console.log("Помилка!! Не можна ділити на 0")
        return;
      }
      break;
  }

  console.log(result);
}

// розкоментуй функцію нижче
// calculate();


// ========================== Task 2 ========================== //


function exponentiation(number, exp) {
  let result = 1;

  while (exp >= 1) {
    result = result * number;
    exp--;
  }

  return result;
}

// розкоментуй функцію нижче
// console.log(exponentiation(3, 3));


// ========================== Task 3 ========================== //

function RockPaperScissors() {
  let array = ["rock", "paper", "scissors"];
  let userChoice = prompt("Rock, paper, scissors?");
  let computerChoice = array[Math.floor(Math.random() * 3)];

  while (userChoice.toLowerCase() === computerChoice) {
    alert(`Your choice: ${userChoice}\nComputer choice: ${computerChoice}\nDRAW! Choose again!`)
    userChoice = prompt("Rock, paper, scissors?");
    computerChoice = array[Math.floor(Math.random() * 3)];
  }

  if (userChoice.toLowerCase() === "rock") {
    switch (computerChoice) {
      case "paper":
        alert(`Your choice: ${userChoice}\nComputer choice: ${computerChoice}\nYOU LOST!`);
        break;
      case "scissors":
        alert(`Your choice: ${userChoice}\nComputer choice: ${computerChoice}\nYOU WIN!`);
        break;
    }
  }

  if (userChoice.toLowerCase() === "paper") {
    switch (computerChoice) {
      case "rock":
        alert(`Your choice: ${userChoice}\nComputer choice: ${computerChoice}\nYOU WIN!`);
        break;
      case "scissors":
        alert(`Your choice: ${userChoice}\nComputer choice: ${computerChoice}\nYOU LOST!`);
        break;
    }
  }

  if (userChoice.toLowerCase() === "scissors") {
    switch (computerChoice) {
      case "paper":
        alert(`Your choice: ${userChoice}\nComputer choice: ${computerChoice}\nYOU WIN!`);
        break;
      case "rock":
        alert(`Your choice: ${userChoice}\nComputer choice: ${computerChoice}\nYOU LOST!`);
        break;
    }
  }
}

// розкоментуй функцію нижче
// RockPaperScissors();


// ========================== Task 1 ========================== //


function getNthFibonacci() {
  let n = +prompt("n?");
  let k = true;
  
  while (k) {
    if (isNaN(n)) {
      alert("Incorrect input")
      n = +prompt("n?");
    } else { break; }
  }

  let n1 = 0, n2 = 1, nextNum;
  for (i = 3; i <= n; i++) {
    nextNum = n1 + n2;
    n1 = n2;
    n2 = nextNum;
  }

  console.log(n2);
}

// розкоментуй функцію нижче
// getNthFibonacci();