// ============================= Primary nav JS ============================= //

const productsBtn = document.querySelectorAll(".primary-nav__subhead");
const primaryNav = document.getElementById("primaryNav");

productsBtn.forEach((item, index) => {
  let block = item.nextElementSibling;
  let returnBtn = item.nextElementSibling.querySelector(".secondary-nav__subhead-show");
  let accordionContent = item.nextElementSibling.querySelector(".accordion-content");
  let accordionList = item.nextElementSibling.querySelector(".accordion-list");

  item.addEventListener("click", () => {
    item.classList.toggle("open");
    block.classList.toggle("open");
  });

  returnBtn.addEventListener("click", () => {
    item.classList.remove("open");
    block.classList.remove("open");
    accordionContent.classList.remove("open");
    accordionList.style.height = "0px";
    accordionContent.querySelector("i").classList.replace("fa-minus", "fa-plus");
  });

  // removeOpen1();
});

// function removeOpen1(index1) {
//   productsBtn.forEach((item2, index2) => {
//     if (index1 != index2) {
//       item2.nextElementSibling.style.top = "-100vh";
//     }
//   })
// }

// ============================= Accordion menu ============================= //

const accordionContent = document.querySelectorAll(".accordion-content");

accordionContent.forEach((item, index) => {
  let header = item.querySelector(".accordion-title");
  header.addEventListener("click", () => {
    item.classList.toggle("open");

    let list = item.querySelector(".accordion-list");
    if (item.classList.contains("open")) {
      list.style.height = `${list.scrollHeight}px`;
      item.querySelector("i").classList.replace("fa-plus", "fa-minus");
    } else {
      list.style.height = "0px";
      item.querySelector("i").classList.replace("fa-minus", "fa-plus");
    }

    removeOpen(index);
  });
});

function removeOpen(index1) {
  accordionContent.forEach((item2, index2) => {
    if (index1 != index2) {
      item2.classList.remove("open");

      let list2 = item2.querySelector(".accordion-list");
      list2.style.height = "0px";
      item2.querySelector("i").classList.replace("fa-minus", "fa-plus");
    }
  });
}
