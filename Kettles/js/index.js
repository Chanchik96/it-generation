const wrapper = document.getElementById("wrapper"),
  slide = document.getElementById("mainSlide"),
  radio = document.getElementById("radio"),
  radiochildren = Array.from(radio.children),
  wrapperChildren = Array.from(wrapper.children);

function changeKettle(color) {
  // function for changing bg, img scr and text src
  document.querySelector("main").style.background = `var(--${color})`;

  slide.children[0].src = `./img/SMEG-${color}.png`;
  slide.children[1].src = `./img/${color}.svg`;

  for (let i = 0; i < 4; i++) {
    // Change Thumbnail background.
    if (wrapper.children[i].classList.contains("active-thumbnail")) {
      wrapper.children[i].classList.remove("active-thumbnail");
    }
  }
  wrapper.children[color].classList.add("active-thumbnail");

  for (let i = 0; i < 4; i++) {
    // Remove radio check
    if (radio.children[i].classList.contains("bx-radio-circle-marked")) {
      radio.children[i].classList.remove("bx-radio-circle-marked");
      radio.children[i].classList.add("bx-radio-circle");
    }
  }

  radio.children[color].classList.add("bx-radio-circle-marked"); // Add radio
}

radiochildren.forEach((child) => {
  child.addEventListener("click", () => {
    changeKettle(child.id);
  });
});

wrapperChildren.forEach((child) => {
  child.addEventListener("click", () => {
    changeKettle(child.id);
  });
});
