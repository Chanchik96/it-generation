const menuBtn = document.querySelector(".hamburger");
const mobileMenu = document.querySelector(".mobile");
const bodyElement = document.querySelector("body");

menuBtn.addEventListener("click", () => {
  menuBtn.classList.toggle("active");
  mobileMenu.classList.toggle("active");
  bodyElement.classList.toggle("active");
  // mobileMenu.style.height = `${mobileMenu.scrollHeight}px`;
});

//close navigation menu when click on window
// document.addEventListener("click", (clickEvent) => {
//   if (
//     clickEvent.target.id !== "primaryNav" &&
//     clickEvent.target.id !== "hamburger"
//   ) {
//     menuBtn.classList.remove("active");
//     mobileMenu.classList.remove("active");
//     bodyElement.classList.remove("active");
//   }
// });
