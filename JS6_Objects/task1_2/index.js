// ============================= Task 1 ============================= //

function task1() {
  const Point = new Object();

  Point.x = +prompt("x?");
  Point.y = +prompt("y?");
  Point.GetQuadrant = function () {
    if (this.x > 0 && this.y > 0) {
      alert("I");
    } else if (this.x > 0 && this.y < 0) {
      alert("II");
    } else if (this.x < 0 && this.y < 0) {
      alert("III");
    } else if (this.x < 0 && this.y > 0) {
      alert("IV");
    }
  };

  Point.GetQuadrant();
}

//task1();  // розкоментуй функцію лівіше, щоб перевірити таск 1 :)


// ============================= Task 2 ============================= //

function task2() {
  const Calculator = new Object();

  Calculator.x = +prompt("x?");
  Calculator.y = +prompt("y?");
  Calculator.sign = prompt("sign + - * / ?");

  Calculator.result = function () {
    const sign = this.sign;
    switch (sign) {
      case "+":
        alert(this.x + this.y);
        break;
      case "-":
        alert(this.x - this.y);
        break;
      case "*":
        alert(this.x * this.y);
        break;
      case "/":
        alert(this.x / this.y);
        break;
      default:
        alert("Please enter one of the sign");
    }
  };

  Calculator.result();
}

//task2(); // розкоментуй функцію лівіше, щоб перевірити таск 2 :)
