let data = localStorage.getItem("data");
EmployeeData = JSON.parse(data);
console.log(EmployeeData);

function showList() {
  let table = document.querySelector("table");
  let data = Object.keys(EmployeeData[0]);
  generateTable(table, EmployeeData);
  generateTableHead(table, data);

  function generateTableHead(table, data) {
    let thead = table.createTHead();
    let row = thead.insertRow();
    for (let key of data) {
      let th = document.createElement("th");
      let text = document.createTextNode(key);
      th.appendChild(text);
      row.appendChild(th);
    }
  }

  function generateTable(table, data) {
    for (let element of data) {
      let row = table.insertRow();
      for (key in element) {
        let cell = row.insertCell();
        let text = document.createTextNode(element[key]);
        cell.appendChild(text);
      }
    }
  }
}

// ============================= Task 5 ============================= //

const sortBtn = document.getElementById("sort");

sortBtn.addEventListener("click", () => {
  let userSelection = prompt(
    "Choose sorting parameter:\n1. Name\n2. Surname\n3. Age\n4. Occupation\n5. Salary"
  );
  switch (userSelection.toLowerCase()) {
    case "name":
      EmployeeData.sort((a, b) => {
        const nameA = a.name.toUpperCase();
        const nameB = b.name.toUpperCase();
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        return 0;
      });
      break;

    case "surname":
      EmployeeData.sort((a, b) => {
        const nameA = a.surname.toUpperCase();
        const nameB = b.surname.toUpperCase();
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        return 0;
      });
      break;

    case "age":
      EmployeeData.sort((a, b) => a.age - b.age);
      break;

    case "occupation":
      EmployeeData.sort((a, b) => {
        const nameA = a.surname.toUpperCase();
        const nameB = b.surname.toUpperCase();
        if (nameA < nameB) {
          return -1;
        }
        if (nameA > nameB) {
          return 1;
        }

        return 0;
      });
      break;

    case "salary":
      EmployeeData.sort((a, b) => a.salary - b.salary);
      break;

    default:
      alert("Please choose one of the given parameters!");
  }
  clearList();
  showList();
});

function clearList() {
  let thead = document.querySelector("thead");
  let tbody = document.querySelector("tbody");
  thead.remove();
  tbody.remove();
}