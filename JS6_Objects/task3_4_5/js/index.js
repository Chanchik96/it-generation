// ============================= Task 3 ============================= //

const nameInput = document.getElementById("name");
const sNameInput = document.getElementById("sName");
const ageInput = document.getElementById("age");
const occupationInput = document.getElementById("occupation");
const submitBtn = document.getElementById("submit");
const showListBtn = document.getElementById("showAllEmployees");

// Set an object prototype
const Employee = {
  name: "Employee's name",
  surname: "Employee's Surname",
  age: "Employee's age",
  occupation: "Employee's occupation",

  show: function () {
    alert(`
    Name:  ${this.name} \n
    Surname:  ${this.surname} \n
    Age:  ${this.age} \n
    Occupation:  ${this.occupation}
    `);
  },
};

let data = localStorage.getItem("data");
let EmployeeData = data ? JSON.parse(data) : [];

submitBtn.addEventListener("click", addNewEmployee);

// Function declarations
function addNewEmployee() {
  const emp = Object.create(Employee);

  if (
    // Check rule if all fields are filled in
    nameInput.value == "" || sNameInput.value == "" || ageInput.value == "" || occupationInput.value == "") {
    return alert("Please fill in all fields");
  }

  emp.name = nameInput.value;
  emp.surname = sNameInput.value;
  emp.age = ageInput.value;
  emp.occupation = occupationInput.value;

  EmployeeData.push(emp);
  localStorage.setItem("data", JSON.stringify(EmployeeData));
  console.log(EmployeeData);
  emp.show();

  clearInputs();
}

function clearInputs() {
  nameInput.value = "";
  sNameInput.value = "";
  ageInput.value = "";
  occupationInput.value = "";
}

// ============================= Task 4 ============================= //

function addSalaryToObject() {
  data = localStorage.getItem("data");
  let EmployeeData = data ? JSON.parse(data) : [];

  EmployeeData.forEach((el) => {
    let occupation = el.occupation.toLowerCase();
    switch (occupation) {
      case "director":
        el.salary = "3000";
        break;
      case "programmer":
        el.salary = "2000";
        break
      case "manager":
        el.salary = "1500";
        break;
      default:
        el.salary = "1000";
    }
  });

  localStorage.setItem("data", JSON.stringify(EmployeeData));
}

// ===================== JS for DevTool buttons ===================== //

const clearLocalStorageBtn = document.getElementById("clearLocalStorage");
const consoleLogEmployeeDataBtn = document.getElementById("consoleLogEmployeeData");
const emptyObjectBtn = document.getElementById("emptyObject");
const logLocalStorageBtn = document.getElementById("logLocalStorage");

clearLocalStorageBtn.addEventListener("click", () => {
  localStorage.clear();
});

emptyObjectBtn.addEventListener("click", () => {
  EmployeeData = [];
});

consoleLogEmployeeDataBtn.addEventListener("click", () => {
  document.querySelector(".showResult").innerHTML = JSON.stringify(EmployeeData);
  console.log(EmployeeData);
});

logLocalStorageBtn.addEventListener("click", () => {
  console.log(localStorage);
});
