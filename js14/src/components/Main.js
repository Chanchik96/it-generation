import { useState, useEffect} from 'react';
import Cards from './Cards';

const Main = () => {
  const [cards, setCards] = useState(null);

  const handleDelete = (name) => {
    const newCards = cards.filter(card => card.name !== name);
    setCards(newCards);
  }

  useEffect(() => {
    fetch('https://swapi.dev/api/people/?format=json')
    .then(res => res.json())
    .then(data => {
      data = data.results
      setCards(data);
    })
  }, []);

  return (
    <div className="cards-container">
      { cards && <Cards cards={ cards } handleDelete={ handleDelete } />}
    </div>
  );
}
 
export default Main
