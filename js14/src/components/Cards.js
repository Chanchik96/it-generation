const Cards = ({cards, handleDelete}) => {
  // cards = cards

  return (
    <div className="card-list">
      { cards.map((card, id) => (
        <div className="card-item" key={id}>
          <h2>Name: { card.name }</h2>
          <p>Height: { card.height }</p>
          <p>Mass: { card.mass }</p>
          <p>Hair color: { card.hair_color }</p>
          <p>Skin color: { card.skin_color }</p>
          <p>Eye color: { card.eye_color }</p>
          <p>Birth year: { card.birth_year }</p>
          <p>Gender: { card.gender }</p>
          <p>URL: <a href={card.url} target="blank">{ card.url }</a></p>
          <button onClick={() => handleDelete(card.name)}>x</button>
        </div>
      ))}
    </div>
  );
}
 
export default Cards;